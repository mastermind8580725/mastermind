import requests

with open("5Letters.txt") as f:
    words = [line.strip() for line in f.readlines()]

url = "https://we6.talentsprint.com/wordle/game"
register = "/register"
create = "/create"
play = "/guess"

response = requests.post(url + register, json={"mode": "mastermind", "name": "we_scholar"})
response_json = response.json()

user_id = response_json["id"]
print("ID is:", user_id)

responsecreating = requests.post(url + create, json={"id":user_id, "overwrite":"True"})
print("Game created")

def guessing(guess :str):
    responseis = requests.post(url +play, json ={"guess":guess, "id":user_id})
    responseis_json = response.json()
    feedback = responseis_json["feedback"]
    print(feedback)
    message = responseis_json["message"]
    print(message)
    return feedback, message

feed, m = guessing("hello")
print(feedback)
print(m)
