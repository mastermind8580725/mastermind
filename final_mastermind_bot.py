import requests
import random
import json

with open("5Letters.txt") as f:
    words = [line.strip() for line in f.readlines()]

url = "https://we6.talentsprint.com/wordle/game"
register = "/register"
create = "/create"
play = "/play"

response = requests.post(url + register, json={"mode": "mastermind", "name": "we_scholar"})
response_json = response.json()

user_id = response_json["id"]

def play_game():
    with open("5Letters.txt", "r") as f:
        words = [line.strip() for line in f.readlines()]

    computer_word = random.choice(words)
    attempts = 0
    possible_words = words.copy()
    required_chars = {i: set('abcdefghijklmnopqrstuvwxyz') for i in range(5)}

    while True:
        bot_word = random.choice(possible_words)
        matches = sum(c1 == c2 for c1, c2 in zip(computer_word, bot_word))
        attempts += 1
	print(f"feedback: {matches}")
	print(f"message: {attempts} attempts made so far")

        if matches == 5:
		print("Win!")
            	print(f"message: {attempts} attempts made so far.")
            	break

        for i, (c1, c2) in enumerate(zip(computer_word, bot_word)):
            if c1 != c2:
                required_chars[i].discard(c2)

        possible_words = [word for word in possible_words if all(c in required_chars[i] for i, c in enumerate(word))]

play_game()


