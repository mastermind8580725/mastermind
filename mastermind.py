import random

def play_game():
    with open("5Letters.txt", "r") as f:
        words = [line.strip() for line in f.readlines()]

    computer_word = random.choice(words)
    attempts = 0
    possible_words = words.copy()
    required_chars = {i: set('abcdefghijklmnopqrstuvwxyz') for i in range(5)}

    while True:
        bot_word = random.choice(possible_words)
        matches = sum(c1 == c2 for c1, c2 in zip(computer_word, bot_word))
        attempts += 1
        print(f"Feedback: {matches}")
        print(f"Message: {attempts} attempts made so far")

        if matches == 5:
            print("Bot wins!")
            print(f"Message: It took {attempts} attempts to guess the word.")
            break

        for i, (c1, c2) in enumerate(zip(computer_word, bot_word)):
            if c1 != c2:
                required_chars[i].discard(c2)

        possible_words = [word for word in possible_words if all(c in required_chars[i] for i, c in enumerate(word))]

play_game()
